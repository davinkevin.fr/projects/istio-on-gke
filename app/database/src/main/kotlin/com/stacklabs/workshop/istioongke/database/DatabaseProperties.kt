package com.stacklabs.workshop.istioongke.database

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URI

@ConstructorBinding
@ConfigurationProperties("database")
class DatabaseProperties(
        val name: String = "database",
        val version: String = "v0",
        val maxLatency: Int = 0,
        val errorRate: Int = 0
)
