package com.stacklabs.workshop.istioongke.middleware

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URI

@ConstructorBinding
@ConfigurationProperties("middleware")
class MiddlewareProperties(
        val name: String = "middleware",
        val databaseUri: URI,
        val version: String = "v0",
        val maxLatency: Int = 0,
        val errorRate: Int = 0
)
