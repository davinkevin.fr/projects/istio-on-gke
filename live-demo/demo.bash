#!/usr/bin/env bash

source .demo-magic.sh

DEMO_PROMPT="\e[38;5;69mΛ\\: \[\e[0;37m\]$ \[\e[0m\]"

# Setup
kubectl apply -f ./manifests/application-base.yml > /dev/null
clear
wait

# Apply mirroring
kubectl apply -f ./manifests/middleware.v2.mirroring.yaml > /dev/null
p "cat manifests/middleware.v2.mirroring.yaml"
bat ./manifests/middleware.v2.mirroring.yaml --style "numbers,grid" --line-range 0:16
wait
clear
bat ./manifests/middleware.v2.mirroring.yaml --style "numbers,grid" --line-range 18:99
wait
clear

p "kubectl apply -f ./manifests/middleware.v2.mirroring.yaml"
echo "virtualservice.networking.istio.io/middleware created"
echo "destinationrule.networking.istio.io/middleware created"
wait
clear

# Canary
p "cat manifests/middleware.v2.canary.yaml"
bat ./manifests/middleware.v2.canary.yaml --style "numbers,grid" --line-range 9:21
wait
pe "kubectl apply -f ./manifests/middleware.v2.canary.yaml"
wait
clear

# Traffic Splitting
kubectl apply -f ./manifests/application-base.yml > /dev/null
kubectl apply -f ./manifests/middleware.v2.traffic-splitting-90-10.yaml > /dev/null

p "kubectl apply -f ./manifests/middleware.v2.traffic-splitting-90-10.yaml"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat manifests/middleware.v2.traffic-splitting-90-10.yaml"
bat ./manifests/middleware.v2.traffic-splitting-90-10.yaml --style "numbers,grid" --line-range 9:18
wait
clear

kubectl apply -f ./manifests/middleware.v2.traffic-splitting-50-50.yaml > /dev/null
p "kubectl apply -f ./manifests/middleware.v2.traffic-splitting-50-50.yaml"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat manifests/middleware.v2.traffic-splitting-50-50.yaml"
bat ./manifests/middleware.v2.traffic-splitting-50-50.yaml --style "numbers,grid" --line-range 9:18
wait
clear

kubectl apply -f ./manifests/middleware.v2.traffic-splitting-0-100.yaml > /dev/null
p "kubectl apply -f ./manifests/middleware.v2.traffic-splitting-0-100.yaml"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat manifests/middleware.v2.traffic-splitting-0-100.yaml"
bat ./manifests/middleware.v2.traffic-splitting-0-100.yaml --style "numbers,grid" --line-range 9:18
wait
clear
