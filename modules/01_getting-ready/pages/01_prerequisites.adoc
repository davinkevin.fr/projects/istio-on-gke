= Prerequisites

[#tools]
== Tools

To attend to this workshop, you will need to be equipped with:

* A working computer
* An internet connection, fast if possible

[#knowledge]
== Knowledge

It will be easier if you already have some skills concerning the following items:

* Be comfortable coding and working with the **terminal**
** Navigating directories and editing files
** Basic bash knowledge (such as creating and manipulating environment variables)
** Being able to issue simple `curl` commands
** Being a `YAML` friendly developer

NOTE: You will be guided throughout the workshop with code snippets and copy-paste ready command lines so relax 😇

* Some basic experience with https://docs.docker.com/install/overview/[**Docker**]
** Knowing how to issue simple `RUN` and `PULL` commands
** Ideally, having some experience with reading and/or writing a **Dockerfile**

NOTE: It's perfectly fine if you do not consider yourself a docker jedi master!

* Be acquainted to Kubernetes
** Understanding the concepts and goals of container orchestration
** Having some practice experience with `kubectl` to deploy services, get logs, describe pods etc...
** Ideally, you have notions about the meaning of **canary deployment**, **A/B testing**, **rolling updates** and
**traffic splitting** and you know it can be difficult to implement

* Be eager to test a cloud environment such as https://cloud.google.com[**Google Cloud Platform**]
** Having already played with it, or with another cloud provider
** Being comfortable working with a remote terminal environment, such as
https://cloud.google.com/shell/[**Google Cloud Shell**]
** Or having some know-how working with cloud identity to issue commands from your computer directly (you will then need
to install `gcloud`, `gsutil`, `kubectl` etc...)

TIP: We recommend you to go with the **Google Cloud Shell approach** to avoid loosing time with the installation of the
required tools

[.next-page]
xref:02_setup-gcp-account.adoc[Sail to next page ⛵]
