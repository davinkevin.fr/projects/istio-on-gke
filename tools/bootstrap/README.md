# Bootstrap

## Requirements
* Terraform `>= v0.12.26`

## One time initial steps
* Create a GCP project (e.g `resource-manager-studio`) ;
* In this project, create a service account that will be used by terraform 
(e.g `terraform@resources-manager-studio.iam.gserviceaccount.com`) ;
* By default, a service account can create up to 10 projects. If you need to create additional projects, you need to 
file a request through [this form](https://support.google.com/code/contact/project_quota_increase). Use the service 
account email address to fill the form. It usually takes Google a couple of days to respond ;
* Create a service account key for this account and download it to a folder of
  your choice ;
* Define the `GOOGLE_CREDENTIALS` environment variable to point to the service account key you just downloaded ; 
* Create a bucket to hold the state (e.g `terraform-state-repository`) in the GCP project ;
* Add the `Storage Object Admin` to the terraform service account you just created on this specific bucket ;
* Create a folder that will hold the generated projects, and get find its ID
* Finally, find your Google billing account ID

## Getting started

* Define the following environment variables with the relevant information:
  ```bash
  Λ\: $ export TF_VAR_google_state_repository_bucket=terraform-state-repository
  
  Λ\: $ export TF_VAR_google_managing_project_id=resource-manager-studio
  
  Λ\: $ export TF_VAR_google_managing_project_region=europe-north1-a
  ```
* Now you may initialize terraform: 
  ```bash
  Λ\: $ terraform init -backend-config "bucket=${TF_VAR_google_state_repository_bucket}"
  ```  
* Congratulations, you are ready to bootstrap multiple projects using terraform ! 👌

## Run
* Create a file to hold the terraform variables, for example:
  ```bash
  Λ\: $ cat << EOF > variables.tfvars
  emails = [
    "attendee@gmail.com"
  ]
  folder_id = "123456789012"
  billing_account = "123456-ABCDEF-A123BC"
  roles = [
    "editor",
    "monitoring.editor",
    "iam.serviceAccountUser",
    "container.admin",
    "compute.admin",
  ]
  apis = [
    "monitoring",
    "compute",
    "cloudtrace",
    "logging",
    "container"
  ]
  EOF
  ```
* Use it with terraform:
  ```bash
  Λ\: $ terraform apply -var-file=variables.tfvars
  ```