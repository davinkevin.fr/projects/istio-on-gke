resource "google_project_service" "apis" {
  count = length(var.emails) * length(var.apis)

  project     = google_project.projects[count.index % length(var.emails)].project_id
  service     = "${element(var.apis, count.index % length(var.apis))}.googleapis.com"
  disable_on_destroy = false
}
