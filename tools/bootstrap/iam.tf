resource "google_project_iam_member" "rights" {
  count = length(var.emails) * length(var.roles)

  project = element(google_project.projects, count.index % length(var.roles)).project_id
  role     = "roles/${element(var.roles, count.index % length(var.roles))}"
  member     = "user:${element(var.emails, count.index % length(var.emails))}"
}
