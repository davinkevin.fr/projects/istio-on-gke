provider "google-beta" {
  project    = var.google_managing_project_id
  region     = var.google_managing_project_region
}

terraform {
  backend "gcs" {
    bucket = var.google_state_repository_bucket
  }
}