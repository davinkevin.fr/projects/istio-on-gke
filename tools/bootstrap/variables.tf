// Context
variable "google_state_repository_bucket" {
  description = "Name of the bucket holding the terraform state"
}

variable "google_managing_project_id" {
  description = "ID of the project running terraform to create other projects aka. the managing project"
}

variable "google_managing_project_region" {
  description = "Default region of the managing project"
}

// Execution
variable "emails" {
  type = list(string)
  description = "List of emails that need GCP projects"
}

variable "folder_id" {
  description = "Parent folder ID for the project"
  type = string
}

variable "billing_account" {
  description = "Billing account to associate the project to"
  type = string
}

variable "roles" {
  type = list(string)
  default = []
  description = "List of roles to affect to users on each project (e.g `editor`, `monitoring.admin`...)"
}

variable "apis" {
  type = list(string)
  default = []
  description = "List of APIs to enable on each project (e.g `monitoring`, `compute`...)"
}