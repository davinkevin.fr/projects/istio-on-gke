# Docker compose

Get application running with `docker-compose` for testing purposes.

## Installation

Follow the [official instructions](https://docs.docker.com/compose/install/).

## Deploy

Start the services:
```
Λ\: $ cd ./tools/docker-compose

Λ\: $ docker-compose up
```

Visit http://localhost:8080:
```
Λ\: $ curl http://localhost:8080; echo;

{"from":"front (v1) => middleware (v1) => database (v1)","date":"2019-12-21T17:17:35.107Z"}
```
