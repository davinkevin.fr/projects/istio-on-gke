#!/usr/bin/env python3
import os
import sys
import argparse

from jinja2 import Environment, FileSystemLoader


class WorkEnvironment:
    @staticmethod
    def from_csv(string: str, email_col: int = 0, password_col: int = 1, project_col: int = 2, separator: str = ','):
        columns = string.split(separator)
        return WorkEnvironment(email=columns[email_col], password=columns[password_col], project=columns[project_col])

    def __init__(self, email: str, password: str, project: str):
        self.email = email
        self.password = password
        self.project = project


def configure_parser(action: argparse._SubParsersAction = None, parser_cmd: str = 'tickets') -> argparse.ArgumentParser:
    parser_kwargs = dict(
        description='''
            Generate a printer-ready HTML template with user credentials tickets.
        '''
    )
    if action is None:
        parser = argparse.ArgumentParser(**parser_kwargs)
    else:
        parser = action.add_parser(name=parser_cmd, **parser_kwargs)

    parser.add_argument(
        dest='input_path',
        help='Path to input CSV file',
    )
    parser.add_argument(
        '-o', '--output-path',
        dest='output_path',
        help='Path to the output path. Defaults to `index.html` in the working directory',
        default=os.path.join(os.getcwd(), 'index.html'),
    )
    parser.add_argument(
        '-t', '--template',
        dest='template',
        help='Path to the template to use. If none provided, a default one is used',
    )
    parser.add_argument(
        '--email-col',
        dest='email_col',
        help='Index of the email column. Defaults to 0',
        default=0,
        type=int
    )
    parser.add_argument(
        '--password-col',
        dest='password_col',
        help='Index of the password column. Defaults to 1',
        default=1,
        type=int
    )
    parser.add_argument(
        '--project-col',
        dest='project_col',
        help='Index of the project column. Defaults to 2',
        default=2,
        type=int
    )
    parser.add_argument(
        '--separator',
        dest='separator',
        help='Separator to use to parse the CSV. Defaults to ","',
        default=','
    )

    return parser


def render_template(work_environments: [WorkEnvironment]):
    environment = Environment(
        loader=FileSystemLoader(
            os.path.join(os.path.dirname(os.path.realpath(__file__)))
        )
    )
    tpl = environment.get_template('index.jinja2.html')
    return tpl.render(work_environments=work_environments)


def run(arguments: argparse.Namespace):
    with open(arguments.input_path, 'r') as stream:
        content = stream.readlines()
        work_environment_list = []
        for line in (raw.strip() for raw in content):
            work_environment_list.append(
                WorkEnvironment.from_csv(
                    string=line,
                    email_col=arguments.email_col,
                    password_col=arguments.password_col,
                    project_col=arguments.project_col,
                    separator=arguments.separator,
                )
            )

    with open(arguments.output_path, 'w') as stream:
        stream.write(render_template(work_environment_list))


if __name__ == '__main__':
    run(arguments=configure_parser().parse_args())


