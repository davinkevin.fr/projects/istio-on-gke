#!/usr/bin/env python3
import argparse

import apis
import credential_tickets
import iam
import projects


def main():
    parser = argparse.ArgumentParser(
        description='''
            Google Cloud Manager is a simple CLI tool automate Google Cloud Projects creation. It is meant to be used
            during workshops and trainings to API enabling, IAM bindings settings and any other necessary "bootstrap"
            operation.  
        '''
    )
    action = parser.add_subparsers(dest='cmd', required=True)
    apis.configure_parser(action=action, parser_cmd='apis')
    credential_tickets.configure_parser(action=action, parser_cmd='tickets')
    iam.configure_parser(action=action, parser_cmd='iam')
    projects.configure_parser(action=action, parser_cmd='projects')

    arguments = parser.parse_args()
    if arguments.cmd == 'apis':
        apis.run(arguments=arguments)
    elif arguments.cmd == 'tickets':
        credential_tickets.run(arguments=arguments)
    elif arguments.cmd == 'iam':
        iam.run(arguments=arguments)
    elif arguments.cmd == 'projects':
        projects.run(arguments=arguments)
    else:
        raise Exception(f'unknown command {arguments.cmd}')


if __name__ == '__main__':
    main()
