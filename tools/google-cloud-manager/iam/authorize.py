#!/usr/bin/env python3
import os
import argparse
from multiprocessing.dummy import Pool as ThreadPool

import google.auth
from googleapiclient import discovery
from google.oauth2 import service_account
from google.api_core.iam import Policy


class WorkEnvironment:
    @staticmethod
    def from_csv(csv_line: str, email_column: int = 0, project_column: int = 1, sep: str = ','):
        email = csv_line.split(sep)[email_column]
        project = csv_line.split(sep)[project_column]

        return WorkEnvironment(email=email, project=project)

    def __init__(self, email: str, project: str):
        self.email = email
        self.project = project


def configure_parser(action: argparse._SubParsersAction = None, parser_cmd: str = 'iam') -> argparse.ArgumentParser:
    parser_kwargs = dict(
        description='''
            Add given project role(s) to given user(s) through IAM policy bindings
        '''
    )
    if action is None:
        parser = argparse.ArgumentParser(**parser_kwargs)
    else:
        parser = action.add_parser(name=parser_cmd, **parser_kwargs)

    parser.add_argument(
        dest='input_path',
        help='Path to input CSV file',
    )
    parser.add_argument(
        '--email-col',
        dest='email_col',
        help='Index of the email column. Defaults to 0',
        default=0,
        type=int
    )
    parser.add_argument(
        '--project-col',
        dest='project_col',
        help='Index of the project column. Defaults to 1',
        default=1,
        type=int
    )
    parser.add_argument(
        '--separator',
        dest='separator',
        help='Separator to use to parse the CSV. Defaults to ","',
        default=','
    )
    parser.add_argument(
        '-s', '--service-account-key',
        dest='service_account_key',
        help='Path to the GCP service account key to use',
        required=True
    )
    parser.add_argument(
        '-r', '--roles',
        action='store',
        dest='roles',
        type=str,
        nargs='+',
        default=[],
        required=True,
        help="Roles to give. Example of usage: -i editor monitoring.admin"
    )
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        help='Number of threads to use to parallelize requests. Defaults to 5',
        type=int,
        default=5
    )
    return parser


def run(arguments: argparse.Namespace):
    with open(os.path.abspath(arguments.input_path), 'r') as stream:
        content = stream.readlines()

    gcp_credentials = service_account.Credentials.from_service_account_file(
        filename=os.path.abspath(arguments.service_account_key)
    )

    pool = ThreadPool(arguments.threads)
    environments = [
        WorkEnvironment.from_csv(
            csv_line=raw.strip(),
            email_column=arguments.email_col,
            project_column=arguments.project_col,
            sep=arguments.separator
        )
        for raw in content
    ]

    def ensure(env: WorkEnvironment):
        try:
            ensure_iam_policies(
                environment=env,
                roles=arguments.roles,
                credentials=gcp_credentials
            )
        except Exception as e:
            print(f'❌ Failed to ensure iam policies on project "{env.project}": {e}')

    pool.map(ensure, environments)


def ensure_iam_policies(
        environment: WorkEnvironment,
        roles: [str],
        credentials: google.oauth2.service_account.credentials.Credentials = None
):
    service = discovery.build('cloudresourcemanager', 'v1', credentials=credentials)

    get_iam_policy_result = service.projects().getIamPolicy(
        resource=environment.project,
    ).execute()

    roles_to_add = list(f'roles/{r}' for r in roles)

    policy = Policy.from_api_repr(get_iam_policy_result)
    for binding in policy.bindings:
        for i, role in enumerate(roles_to_add):
            if role != binding.get('role'):
                continue
            if not binding.get('members'):
                binding['members'] = []
            binding['member'].append(f'user:{environment.email}')
            roles_to_add.pop(i)

    for role in roles_to_add:
        policy.bindings.append({
            'role': role,
            'members': [f'user:{environment.email}']
        })

    print(f'👷 About to add {len(roles)} role(s) on project "{environment.project}" to user "{environment.email}"')
    service.projects().setIamPolicy(
        resource=environment.project,
        body={
            'policy': policy.to_api_repr()
        },
    ).execute()
    print('✅ Done')


if __name__ == '__main__':
    run(arguments=configure_parser().parse_args())
