import argparse

from .create import configure_parser as create_configure_parser
from .create import run as create_run

from .delete import configure_parser as delete_configure_parser
from .delete import run as delete_run


def configure_parser(action: argparse._SubParsersAction = None, parser_cmd: str = 'projects') -> argparse.ArgumentParser:
    parser_kwargs = dict(
        description='''
           Manage Google Cloud project(s)
        '''
    )
    if action is None:
        parser = argparse.ArgumentParser(**parser_kwargs)
    else:
        parser = action.add_parser(name=parser_cmd, **parser_kwargs)

    action = parser.add_subparsers(dest='cmd_projects', required=True)
    create_configure_parser(action=action, parser_cmd='create')
    delete_configure_parser(action=action, parser_cmd='delete')

    return parser


def run(arguments: argparse.Namespace):
    if arguments.cmd_projects == 'create':
        create_run(arguments=arguments)
    elif arguments.cmd_projects == 'delete':
        delete_run(arguments=arguments)
    else:
        raise Exception(f'unknown command {arguments.cmd_projects}')