# Terraform

_This folder shows how to use terraform to deploy the demo apps locally using
docker, as an alternative to `docker-compose`_

## Initialize

```sh
Λ\: $ terraform init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "docker" (terraform-providers/docker) 2.7.1...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.docker: version = "~> 2.7"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

## Deploy

```sh
Λ\: $ terraform apply -auto-approve

docker_image.front: Creating...
docker_image.middleware: Creating...
docker_image.database: Creating...
docker_image.middleware: Creation complete after 1s [id=sha256:798bdd166379d1e112aadfe057e7c6a9a76258a489832af936959ca002a45421stacklabs/istio-on-gke-middleware]
docker_image.database: Creation complete after 1s [id=sha256:c67f33725e96b1207daf480b11b970122ee485968e8cef8cb1a80680a0f14c1bstacklabs/istio-on-gke-database]
docker_image.front: Creation complete after 1s [id=sha256:3c6a46010226d60dc65eaf3a4e16366addc953f53c7c398d0da648b1e3e663dastacklabs/istio-on-gke-front]
docker_container.database: Creating...
docker_container.database: Creation complete after 1s [id=6804bff525acf343ef990bcf2feb867c16764908cce7a0af359a3f5590ca2639]
docker_container.middleware: Creating...
docker_container.middleware: Creation complete after 0s [id=084c0e9c887ba4c2b5d75cb77b7cdb667a6c74d9016f72cbcafcc098359869e1]
docker_container.front: Creating...
docker_container.front: Creation complete after 1s [id=971135e77314387a5a706fdc399bee44b8c44bf6c8053c274f9123da6abcbc53]

Warning: "links": [DEPRECATED] The --link flag is a legacy feature of Docker. It may eventually be removed.

  on front.tf line 6, in resource "docker_container" "front":
   6: resource "docker_container" "front" {

(and one more similar warning elsewhere)


Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

## Clean

```sh
Λ\: $ terraform destroy -auto-approve

docker_image.middleware: Refreshing state... [id=sha256:798bdd166379d1e112aadfe057e7c6a9a76258a489832af936959ca002a45421stacklabs/istio-on-gke-middleware]
docker_image.front: Refreshing state... [id=sha256:3c6a46010226d60dc65eaf3a4e16366addc953f53c7c398d0da648b1e3e663dastacklabs/istio-on-gke-front]
docker_image.database: Refreshing state... [id=sha256:c67f33725e96b1207daf480b11b970122ee485968e8cef8cb1a80680a0f14c1bstacklabs/istio-on-gke-database]
docker_container.database: Refreshing state... [id=6804bff525acf343ef990bcf2feb867c16764908cce7a0af359a3f5590ca2639]
docker_container.middleware: Refreshing state... [id=084c0e9c887ba4c2b5d75cb77b7cdb667a6c74d9016f72cbcafcc098359869e1]
docker_container.front: Refreshing state... [id=971135e77314387a5a706fdc399bee44b8c44bf6c8053c274f9123da6abcbc53]
docker_container.front: Destroying... [id=971135e77314387a5a706fdc399bee44b8c44bf6c8053c274f9123da6abcbc53]
docker_container.front: Destruction complete after 1s
docker_image.front: Destroying... [id=sha256:3c6a46010226d60dc65eaf3a4e16366addc953f53c7c398d0da648b1e3e663dastacklabs/istio-on-gke-front]
docker_image.front: Destruction complete after 0s
docker_container.middleware: Destroying... [id=084c0e9c887ba4c2b5d75cb77b7cdb667a6c74d9016f72cbcafcc098359869e1]
docker_container.middleware: Destruction complete after 0s
docker_image.middleware: Destroying... [id=sha256:798bdd166379d1e112aadfe057e7c6a9a76258a489832af936959ca002a45421stacklabs/istio-on-gke-middleware]
docker_image.middleware: Destruction complete after 0s
docker_container.database: Destroying... [id=6804bff525acf343ef990bcf2feb867c16764908cce7a0af359a3f5590ca2639]
docker_container.database: Destruction complete after 1s
docker_image.database: Destroying... [id=sha256:c67f33725e96b1207daf480b11b970122ee485968e8cef8cb1a80680a0f14c1bstacklabs/istio-on-gke-database]
docker_image.database: Destruction complete after 0s

Warning: "links": [DEPRECATED] The --link flag is a legacy feature of Docker. It may eventually be removed.

  on front.tf line 6, in resource "docker_container" "front":
   6: resource "docker_container" "front" {

(and one more similar warning elsewhere)


Destroy complete! Resources: 6 destroyed.
```