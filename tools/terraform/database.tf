resource "docker_image" "database" {
  name         = "stacklabs/istio-on-gke-database"
  keep_locally = true
}

resource "docker_container" "database" {
  image = docker_image.database.latest
  name  = "database"
}