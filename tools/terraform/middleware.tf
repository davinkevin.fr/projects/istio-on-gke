resource "docker_image" "middleware" {
  name         = "stacklabs/istio-on-gke-middleware"
  keep_locally = true
}

resource "docker_container" "middleware" {
  image = docker_image.middleware.latest
  name  = "middleware"
  links = toset([
    docker_container.database.name
  ])
  env = [ "MIDDLEWARE_DATABASE_URI=http://database:8080" ]
}